﻿using System;
using BencodeNET.Parsing;
using BencodeNET.Torrents;
using CommandLine;

namespace SimpleTorrentClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<TorrentClientOptions>(args)
                .WithParsed<TorrentClientOptions>(o => {
                    var parser = new BencodeParser();
                    var torrent = parser.Parse<Torrent>(o.FilePath);

                    if(o.Mode == ExecutionMode.Download)
                    {

                    }
                    else if(o.Mode == ExecutionMode.Info)
                    {
                        var fileSizeInMb = torrent.TotalSize / 1024 / 1024;
                        Console.WriteLine($"Size: {fileSizeInMb}mb Mode:{torrent.FileMode}");
                    }

                    Console.WriteLine(torrent.Trackers[0][0]);
                });
        }
    }
}
