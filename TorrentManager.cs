using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using BencodeNET.Torrents;

public class TorrentManager
{
    private readonly HttpClient _client = new HttpClient();

    public async Task<HttpResponseMessage> GetPeers(Torrent torrent)
    {
        var infoHash = torrent.GetInfoHash();
        var url = GetTrackerUrl(torrent);

        var res = await _client.GetAsync(url);
        
        return res;
    }

    private string GetFullUrl(Torrent torrent)
    {
        var baseUrl = GetTrackerUrl(torrent);
        var builder = new UriBuilder(baseUrl);

        var query = HttpUtility.ParseQueryString(builder.Query);

        return builder.ToString();
    }

    private string GetTrackerUrl(Torrent torrent)
    {
        return torrent.Trackers[0][0];
    }
}