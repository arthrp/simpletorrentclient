using CommandLine;

public enum ExecutionMode
{
    Info, Download
}

public class TorrentClientOptions
{
    [Option('m', Default = ExecutionMode.Info)]
    public ExecutionMode Mode { get; set; }

    [Option('p')]
    public string FilePath { get; set; }
}